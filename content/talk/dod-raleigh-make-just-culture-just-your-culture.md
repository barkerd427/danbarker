+++
date = "2019-10-01T00:00:00"
title = "Make Just Culture Just Your Culture"
abstract = "The DevOps movement has been bringing Just Culture into the tech industry. It’s time to learn what it is and how you can harness it to make your teams happier. We’ll discuss some of the research from Sidney Dekker and the DevOps movement, and I’ll share some of the methods I’ve used during cultural transformations."
abstract_short = ""
event = "DevOpsDays Raleigh"
event_url = "https://devopsdays.org/events/2019-raleigh/"
location = "Raleigh, North Carolina"

selected = false
math = true

url_pdf = "https://drive.google.com/open?id=1-GfcXuA0w8YqUlxQoEUdJWWF1jrdb7Ij"
url_slides = "https://www.slideshare.net/DanielBarker4/make-just-culture-just-your-culture-devopsdays-raleigh"
url_video = ""

# Optional featured image (relative to `static/img/` folder).
[header]
image = "headers/bubbles-wide.jpg"
caption = "My caption :smile:"

+++

<iframe src="//www.slideshare.net/slideshow/embed_code/key/huywJf7bhBvxds" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/DanielBarker4/make-just-culture-just-your-culture-devopsdays-raleigh" title="Make just culture just your culture devopsdays raleigh" target="_blank">Make just culture just your culture devopsdays raleigh</a> </strong> from <strong><a href="https://www.slideshare.net/DanielBarker4" target="_blank">Daniel Barker</a></strong> </div>

Just Culture stands to benefit the tech industry and all industries greatly. It started in aviation and health care, but John Allspaw brought it into the tech field through DevOps. It has actually become one of the three pillars of DevOps according to Gene Kim and John Willis.

I have been studying this field for years, and we’ll discuss some of the original research as well as some of the latest related to DevOps. Then we’ll discuss some of the methods I’ve used in transformations that have included culture. In fact, all transformations are underpinned by culture, and that is something I always focus on first.
