+++
date = "2019-10-02T00:00:00"
title = "Getting Started with AI For Free!"
abstract = "Curious about AI? Find out how you can get started today! We’ll walk through several tools that are open source and commonly used in data science. I’ll also provide information about the algorithms being used and we’ll walk through a few different use-cases so you’ll better understand them."
abstract_short = ""
event = "DevOpsDays Raleigh"
event_url = "https://devopsdays.org/events/2019-raleigh/"
location = "Raleigh, North Carolina"

selected = false
math = true

url_pdf = "https://drive.google.com/open?id=1nHabZEIqxUzrG1wHhlLqIF0OjeyBhs48"
url_slides = "https://www.slideshare.net/DanielBarker4/getting-started-with-ai-for-free-devopsdays-rdu"
url_video = ""

# Optional featured image (relative to `static/img/` folder).
[header]
image = "headers/bubbles-wide.jpg"
caption = "My caption :smile:"

+++

<iframe src="//www.slideshare.net/slideshow/embed_code/key/aJ34kVDQFEAXqv" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/DanielBarker4/getting-started-with-ai-for-free-devopsdays-rdu" title="Getting started with ai for free devopsdays rdu" target="_blank">Getting started with ai for free devopsdays rdu</a> </strong> from <strong><a href="https://www.slideshare.net/DanielBarker4" target="_blank">Daniel Barker</a></strong> </div>

AI is a hot topic, but it can feel out of reach for many. I’m an engineer and sysadmin who has learned data science, and it helps me in both of the other areas. I’ll be sharing what I’ve learned and how it relates to attendees’ jobs. I’ll bring the topic to an operational level that doesn’t feel out of reach anymore, and will actually allow attendees to immediately try out these new tools.

I’ll be using open source tools primarily in Python and Node to show just how easy it can be to use advanced algorithms to discover insights in data. We’ll also use several open datasets that attendees will have access to use. Then I’ll explain each algorithm used under the hood of the tool and what types of problems it can solve. I’ll demystify the field so we can have more domain experts getting involved with data science.
