+++
date = "2019-06-07T00:00:00"
title = "Getting Started with AI For Free!"
abstract = "Curious about AI? Find out how you can get started today! We’ll walk through several tools that are open source and commonly used in data science. I’ll also provide information about the algorithms being used and we’ll walk through a few different use-cases so you’ll better understand them."
abstract_short = ""
event = "Refactr Tech"
event_url = "https://refactr.tech"
location = "Atlanta, Georgia"

selected = false
math = true

url_pdf = ""
url_slides = ""
url_video = "https://vimeo.com/354800799"

# Optional featured image (relative to `static/img/` folder).
[header]
image = "headers/bubbles-wide.jpg"
caption = "My caption :smile:"

+++

<iframe src="https://player.vimeo.com/video/354800799?title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/354800799">Refactr2019 - Getting Started with Ai for Free - Dan Barker</a> from <a href="https://vimeo.com/recallact">RecallAct</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

AI is a hot topic, but it can feel out of reach for many. I’m an engineer and sysadmin who has learned data science, and it helps me in both of the other areas. I’ll be sharing what I’ve learned and how it relates to attendees’ jobs. I’ll bring the topic to an operational level that doesn’t feel out of reach anymore, and will actually allow attendees to immediately try out these new tools.

I’ll be using open source tools primarily in Python and Node to show just how easy it can be to use advanced algorithms to discover insights in data. We’ll also use several open datasets that attendees will have access to use. Then I’ll explain each algorithm used under the hood of the tool and what types of problems it can solve. I’ll demystify the field so we can have more domain experts getting involved with data science.
