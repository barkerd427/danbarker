+++
date = "2019-08-20T00:00:00"
title = "Understanding Risk Can Fund Transformation"
abstract = """Risk quantification can be a valuable tool for selling transformation to executives. It’s also important to understand how your company looks at risk. Most CEOs will have a certain amount of risk they’re willing to take called their risk tolerance. This will help you to understand if your project is worth pursuing. If your project won’t offset more risk than the risk tolerance, then it’s unlikely to be funded if it’s not a new feature or product.

We’ll explore how we can go about quantifying risk in real-world situations I’ve faced in presenting transformation initiatives. This information will help you to understand how a business looks at risk and the associated value of mitigating that risk. Want to start a new CI/CD initiative, bake in the risk averted by putting SAST and DAST in your pipeline. This would have helped Equifax avoid their breach and the risk of such a breach is very high and carries a very large cost.

We’ll also take a look at some additional resources to help you assess risk such as data on breaches, attacks, the FAIR tool, and other resources you can use once you leave the session."""
abstract_short = "Risk quantification can be a valuable tool for selling transformation to executives. It’s also important to understand how your company looks at risk. Most CEOs will have a certain amount of risk they’re willing to take called their risk tolerance. This will help you to understand if your project is worth pursuing. If your project won’t offset more risk than the risk tolerance, then it’s unlikely to be funded if it’s not a new feature or product."
event = "DevOpsDays Dallas"
event_url = "https://devopsdays.org/events/2019-dallas/"
location = "Dallas, Texas"

selected = false
math = true

url_pdf = "https://drive.google.com/open?id=150JMoxdVMyj9dTJI7uFvf5Beh05RBiFa"
url_slides = "https://docs.google.com/presentation/d/1qka8IRYdpBKKJV8v868rvH7AZe-O1v-HEjtd35MEBDo/edit?usp=sharing"
url_video = ""

# Optional featured image (relative to `static/img/` folder).
[header]
image = "headers/bubbles-wide.jpg"
caption = "My caption :smile:"

+++

<iframe src="//www.slideshare.net/slideshow/embed_code/key/6MuJOjv8YTfP2f" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/DanielBarker4/understanding-risk-can-fund-transformation-dod-dallas" title="Understanding Risk Can Fund Transformation - DOD Dallas" target="_blank">Understanding Risk Can Fund Transformation - DOD Dallas</a> </strong> from <strong><a href="https://www.slideshare.net/DanielBarker4" target="_blank">Daniel Barker</a></strong> </div>
