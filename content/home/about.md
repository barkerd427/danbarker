+++
# About/Biography widget.

date = "2016-04-20T00:00:00"
draft = false

widget = "about"

# Order that this section will appear in.
weight = 1

# List your academic interests.
[interests]
  interests = [
    "Cloud Computing",
    "Human Factors",
    "Machine Learning",
    "DevOps",
    "Continuous Improvement"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "MS Computer Science - Data Science"
  institution = "University of Illinois - Urbana-Champaign"
  year = 2020
[[education.courses]]
  course = "BS in Computer Science"
  institution = "University of Maryland, University-College"
  year = 2012

+++

# Biography

Dan spent 12 years in the military as a mechanic on fighter jets, like the F-16, before transitioning to a career in technology as a Software Engineer, then a DevOps Engineer, a Software Development Manager, a Chief Architect, and now a Senior Director at Red Hat. He's leading the engineering and architecture organization for the Enterprise Data and Analytics team at Red Hat. Dan is also an organizer of the DevOps KC Meetup and the DevOpsDays KC conference.
